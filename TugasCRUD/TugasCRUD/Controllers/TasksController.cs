﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.Common;

using TugasCRUD.Models;

namespace TugasCRUD.Controllers
{
    [Route("api/tasks")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private TasksDB db;

        public TasksController(IConfiguration config)
        {
            string connString = config["ConnectionStrings:Default"];
            db = new TasksDB(connString);
        }

        [HttpPost]
        [Route("AddUserWithTask")]
        public ActionResult AddUserWithTask([FromBody] UserModel user)
        {
            try
            {
                db.AddUserWithTask(user);
                return Ok();
            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("GetUserWithTask")]
        public ActionResult GetUserWithTask()
        {
            try
            {
                List<UserModel> result = db.GetUserWithTask();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
