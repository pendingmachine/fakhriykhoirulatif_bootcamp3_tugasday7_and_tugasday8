﻿using System.Data;
using System.Data.SqlClient;

namespace TugasCRUD.Models
{
    public class TasksDB : BaseDB
    {
        public TasksDB(string connString) : base(connString)
        {
        }

        public void AddUserWithTask(UserModel user)
        {
            string userInsertQuery = "INSERT INTO Users([name]) VALUES (@Name); SELECT IDENT_CURRENT('Users');";
            string taskInsertQuery = "INSERT INTO Tasks(task_detail, fk_users_id) VALUES (@TaskDetail, @UserId);";

            using (SqlConnection conn = GetConnection())
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        cmd.CommandText = userInsertQuery;
                        cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar) { Value = user.name ?? "" });

                        decimal userId = (decimal)cmd.ExecuteScalar();
                        cmd.Parameters.Clear();

                        cmd.CommandText = taskInsertQuery;
                        foreach (TaskModel task in user.tasks)
                        {
                            cmd.Parameters.Add(new SqlParameter("@TaskDetail", SqlDbType.VarChar) { Value = task.task_detail ?? "" });
                            cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int) { Value = userId });

                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }

                        cmd.Transaction.Commit();
                        conn.Close();
                    } catch (Exception e)
                    {
                        cmd.Transaction.Rollback();
                        conn.Close();
                        throw;
                    }
                }
            }
        }

        public List<UserModel> GetUserWithTask()
        {
            List<UserModel> result = new List<UserModel>();
            string userQuery = "SELECT pk_users_id, [name] FROM Users;";
            string tasksQuery = "SELECT pk_tasks_id, task_detail FROM Tasks WHERE fk_users_id = @UserId;";
            DataTable userData = ExecuteQuery(userQuery);

            foreach (DataRow userRow in userData.Rows)
            {
                List<TaskModel> tasks = new List<TaskModel>();
                int userId = (int)userRow["pk_users_id"];
                string userName = (string)userRow["name"];

                SqlParameter[] tasksParams =
                {
                    new SqlParameter("@UserId", SqlDbType.Int) { Value = userId }
                };

                DataTable tasksData = ExecuteQuery(tasksQuery, tasksParams);

                foreach (DataRow taskRow in tasksData.Rows)
                {
                    TaskModel task = new TaskModel
                    {
                        pk_tasks_id = (int)taskRow["pk_tasks_id"],
                        task_detail = (string)taskRow["task_detail"]
                    };
                    tasks.Add(task);
                }

                UserModel user = new UserModel
                {
                    pk_users_id = userId,
                    name = userName,
                    tasks = tasks
                };
                result.Add(user);
            }

            return result;
        }
    }
}
