﻿using System.Data;
using System.Data.SqlClient;

namespace TugasCRUD.Models
{
    public class BaseDB
    {
        private string ConnString;

        public BaseDB(string connString)
        {
            ConnString = connString;
        }

        public DataTable ExecuteQuery(string query, SqlParameter[] sqlParams = null)
        {
            DataTable result = new DataTable();

            using (SqlConnection conn = GetConnection())
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (sqlParams != null) cmd.Parameters.AddRange(sqlParams);

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(result);
                }

                conn.Close();
            }

            return result;
        }

        public void ExecuteNonQuery(string query, SqlParameter[] sqlParams = null)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (sqlParams != null) cmd.Parameters.AddRange(sqlParams);

                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        public object ExecuteScalar(string query, SqlParameter[] sqlParams = null)
        {
            object result = null;

            using (SqlConnection conn = GetConnection())
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    if (sqlParams != null) cmd.Parameters.AddRange(sqlParams);

                    result = cmd.ExecuteScalar();
                }
            }

            return result;
        }

        public SqlConnection GetConnection()
        {
            return new SqlConnection(ConnString);
        }
    }
}
